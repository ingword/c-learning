﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        interface IMutable
        {
            void SetX(int v);
        }
        struct Vector : IMutable
        {
            public int x;
            public void SetX(int v) { x = v; }
        }
        static void Main(string[] args)
        {
            var onStack = new Vector();
            onStack.x = 42;
            
            var inHeap = (object)onStack;

            Console.WriteLine("Hello World");
            Console.WriteLine("Vector.X = {0}", onStack.x);
            Console.WriteLine("onHeap.X = {0}", ((Vector)inHeap).x);
            onStack.x += 42;
            Console.WriteLine("onStack.x += 42;");
            Console.WriteLine("Vector.X = {0}", onStack.x);
            Console.WriteLine("onHeap.X = {0}", ((Vector)inHeap).x);
            ((IMutable)inHeap).SetX(11);
            Console.WriteLine("((IMutable)inHeap).SetX(11);");
            Console.WriteLine("Vector.X = {0}", onStack.x);
            Console.WriteLine("(Vector)inHeap).x = {0}", ((Vector)inHeap).x);

            Console.ReadLine();
        }
    }
}
