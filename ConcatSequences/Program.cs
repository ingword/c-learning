﻿using System;
using System.Collections.Generic;

namespace ConcatSequences
{
    static class Program
    {
        public static IEnumerable<T> Concat<T>
        (
            IEnumerable<T> first, IEnumerable<T> second
        )
        {
            foreach (var t in first)
            {
                yield return t;
            }
            foreach (var t in second)
            {
                yield return t;
            }

        }
        static void Main(string[] args)
        {
            var a = new[] { 1, 2, 3, 4};
            var b = new[] { 2, 3, 4, 5 };
            foreach (int i in Concat(a, b))
            {
                Console.Write($" {i}");
            }

            
        }
    }
}
