﻿using System;

namespace ConsoleApp2
{
	public class Program
	{

		public delegate double myMethodDelegate(double myVar);

		internal class Test
        {
			private long hugeNumber;
			public long HugeNumber   // property
			{
				get { System.Console.Write("get! ");  return hugeNumber; }
				set { hugeNumber = value; System.Console.Write("set! "); }
			}
			internal double Get(double v)
            {
				return v * v;
            }

			internal double AnotherGet(double v)
            {
				return v * v * v;
            }



		}

		public static void Main()
		{
			//var test = new Test();
			//var rand = new Random();
			
			//myMethodDelegate DelegateGet = new myMethodDelegate(test.Get);

			//Console.WriteLine("DelegateGet - test.Get {0}", DelegateGet(3));
			//DelegateGet = test.AnotherGet;
			//Console.WriteLine("DelegateGet - test.AnotherGet {0}", DelegateGet(3));
			//DelegateGet = delegate (double v)
			//	{
			//		return v * v * v * v;
			//	};
			//Console.WriteLine("DelegateGet - anonymous {0}", DelegateGet(3));

			Action zig = Bar();
			Action zag = Bar();

			short foo = zig();
			foo = zag();
			foo = zig();
			foo = zag();

			System.Console.WriteLine(foo);
			//System.Console.ReadKey();


		}
	}
}
