﻿using System;
using System.Collections.Generic;

namespace DynamicList
{
    class Program
    {
        public static void Main()
        {
            dynamic items = new List<int> { 1, 2, 3 };
            dynamic valueToAdd = 2;

            foreach (dynamic item in items)
            {
                string sum = item + valueToAdd;
                Console.Write(sum);
            }
        }
	}
}
