﻿using System;
using System.Collections;
using System.IO;

namespace EventsExample
{
    public class Button
    {
        public event System.Action _OnEvent;
        public event System.Action OnEvent 
        {
            add { _OnEvent += value; System.Console.Write("add!"); }
            remove { _OnEvent -= value; System.Console.Write("remove!"); }
        }

        internal void FireEvent()
        {
            _OnEvent?.Invoke();
        }
    }

    public class FTP : IDisposable
    {
        bool disposed = false;
        public void Foo()
        {
            Console.WriteLine("Foo");
        }
        public void Dispose()
        {
            //if (disposed) { return; }
            Console.WriteLine(" Dispose");
            //disposed = true;
            //GC.SuppressFinalize(this);
        }

        public static void DoOddStuff()
        {
            const string fileName = "tmp.tmp";

            byte[] dataToWrite = new byte[] { 0, 1, 0, 1 };

            using(FileStream file = new FileStream(fileName, FileMode.Create))
            {
                file.Write(dataToWrite, 0, dataToWrite.Length);

                file.Dispose();
            }

            File.Delete(fileName);
        }

        ~FTP()
        {
            Dispose();
            Console.WriteLine("Finalize");
        }
    }
    class Program
    {
        private static void Display<T>(System.Collections.Generic.IEnumerable<T> values)
        {
            // ваш код перебора последовательности
            var Enumerator = values.GetEnumerator();
            int i = 0;

            while ((i <= 9) && Enumerator.MoveNext())
            {
                string tmpStr = Enumerator.Current?.ToString();
                System.Console.Write(tmpStr);
                i++;
            }
            Enumerator.Dispose();

        }
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            string[] stringArray = { "2", "3", "5", "7", "11", "13", "17", "19", "23", "29" };
            Display(stringArray);

            Button tmpBtn = new Button();

            tmpBtn._OnEvent += delegate
            {
                Console.Write("add delegate");
            };

            tmpBtn.FireEvent();
        }
    }
}
