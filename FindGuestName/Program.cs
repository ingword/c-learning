﻿using System;
using System.Linq;

namespace FindGuestName
{
    class Program
    {
        private static string[] guestsArray = new[]
            {"Ivan Petrov", "Anton Pavlov", "Mircea Cioban", "Sergio Mascarpone", "Daniela Petroff", "Anna Popa"};

        private static bool IsGuestInvited(string[] guestStrings, string guestName)
        {
            return guestsArray.FirstOrDefault(s => s.Contains(guestName)) != null;
        }
        public static void Main(string[] args)
        {
           Console.WriteLine($"Guest is invited - ${IsGuestInvited(guestsArray, "Anna")}");
        }
    }
}
