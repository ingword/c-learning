﻿using System;
using System.Reflection.Metadata.Ecma335;

namespace Linq1._12
{
    public static class Program
    {
        sealed class Track
        {
            internal double Length { get; set; }
        }

        private static bool CompareWithEpsilon(this double self, double reference, double epsilon)
        {
            if (Math.Abs(self - reference) <= epsilon)
            {
                return true;
            }

            return false;
        }

        public static void Main()
        {
            double a = 4000000000.0;

            Predicate<double> aboveTheLimit = x => x > 300000.0;

            Console.Write(a.CompareWithEpsilon(4.9, 0.1));
            Console.Write(aboveTheLimit(a));
        }
    }
}
