﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace Linq2
{

    public static class Program
    {
        public static IEnumerable<TP> SelectMany<TS, TP>
        (
        this IEnumerable<TS> source, 
        Func<TS, IEnumerable<TP>> produce
        )
        {
            foreach (var se in source)
            {
                foreach (var p in produce(se))
                {
                    if (se == null || p == null) { yield break;}
                    yield return p;
                }
            }
        }

        private static IEnumerable<int> MyFunc(int arg)
        {
            List<int> List = new List<int>();
            List.Add(arg);
            List.Add(arg*arg);
            List.Add(arg*2);
            return List;

        }

        static int[] inputs = {7, 13, 2};

        static void Main(string[] args)
        {
            var items = inputs.SelectMany(MyFunc);
            
            foreach (var item in items)
            {
                Console.Write($" {item}");
            }
        }

    }
}
