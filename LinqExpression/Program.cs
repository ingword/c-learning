﻿using System;
using System.Linq;
using System.Net.Sockets;

namespace LinqExpression
{
    class Program
    {
        internal sealed class RockStar
        {
            internal string Name { get; set; }
            internal string CurrentBand { get; set; }
            internal string Role { get; set; }
            internal long SalaryPerGig { get; set; }
        }

        static void Main(string[] args)
        {
            var stars = new[]
            {
                new RockStar { Name = "Ian Gillan",  CurrentBand = "Deep Purple", SalaryPerGig = 700, Role = "Vocal" },
                new RockStar { Name = "Angus Young",  CurrentBand = "AC/DC", SalaryPerGig = 900, Role = "Guitar" },
                new RockStar { Name = "Roger Glover",  CurrentBand = "Deep Purple", SalaryPerGig = 400, Role = "Bass" },
                new RockStar { Name = "David Coverdale",  CurrentBand = "Whitesnake", SalaryPerGig = 950, Role = "Vocal" },
                new RockStar { Name = "Rick Allen",  CurrentBand = "Def Leppard", SalaryPerGig = 800, Role = "Drums" },
                new RockStar { Name = "Steven J. Morse",  CurrentBand = "Deep Purple", SalaryPerGig = 600, Role = "Guitar" },
                new RockStar { Name = "Don Airey",  CurrentBand = "Deep Purple", SalaryPerGig = 600, Role = "Keyboards" },
            };

            //var orderedStars = from star in stars where star.CurrentBand == "Deep Purple" select (new {Name = star.Name, Instrument = star.Role});
            
            var orderedStars = 
                    from star in stars 
                    group star.CurrentBand by star.CurrentBand into groupStar
                    select (new { Name = groupStar.Key, Size = groupStar.Count() });

            foreach (var orderedStar in orderedStars)
            {
                Console.Write($"{orderedStar} ");
            }
            

        }
    }
}
