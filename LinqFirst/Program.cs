﻿using System;

namespace LinqFirst
{
    class Program
    {
        static void Main(string[] args)
        {
            var MyType = new
            {
                Name = "MyName ",
                Age = 21,
            };

            var monsters = new[]
            {
                new
                {
                    Name = "Anders Hejlsberg",
                    Level = 1
                },
                new
                {
                    Name = "Eric Lippert",
                    Level = 2
                },
                new
                {
                    Name = "Luca Cardelli",
                    Level = 3
                },
                new
                {
                    Name = "Erik Meijer",
                    Level = 4
                }
            };

            var frankenstein = new
            {
                monsters[0].Name,
                monsters[1].Level,
                Strength = 0

            };

            foreach (var item in monsters)
            {
                Console.WriteLine(item.Name);
            }
            Console.WriteLine($"Name = {frankenstein.Name}, Age = {frankenstein.Level}, Strength = {frankenstein.Strength}");
        }
    }
}
