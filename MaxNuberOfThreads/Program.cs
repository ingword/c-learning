﻿using System;
using System.Threading;

namespace MaxNuberOfThreads
{
    class Program
    {
        static void Main(string[] args)
        {
            int workerThreadsCount, ioThreadsCount;
            ThreadPool.GetMaxThreads(out workerThreadsCount, out ioThreadsCount);
            Console.WriteLine($"workerThreadsCount - {workerThreadsCount}, ioThreadsCount - {ioThreadsCount}");
        }
    }
}
