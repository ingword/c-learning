﻿using System;
using System.Collections.Generic;

namespace PolyLine
{
    class PolyLine
    {
        public List<int> _data;

        public IEnumerable<int> GetElementsMoreThanZero()
        {
                foreach (int i in _data)
                {
                    if (i > 0)
                    {
                        yield return i;
                    }
                }
        }

        public static System.Collections.Generic.IEnumerable<long> Even()
        {
            // ваша реализация
            for (int i = 0; i < long.MaxValue; i++)
            {
                if (i % 2 == 0)
                {
                    yield return i;
                }
            }

        }

        public PolyLine()
        {
            _data = new List<int>();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var rnd = new Random();
            var pl = new PolyLine();
            for (int i = 0; i <= 99; i++)
            {
                pl._data.Add(rnd.Next(-150, 150));
            }

            int count = 0;
            foreach (long i in PolyLine.Even())
            {
                Console.Write($" {i}");
                count++;
            }
            Console.WriteLine("");
            Console.WriteLine($"Count - {pl._data.Count}");
            Console.WriteLine($"GetElementsMoreThanZero Count - {count}");
        }
    }
}
