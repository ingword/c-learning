﻿using System;
using System.Text.RegularExpressions;

namespace RegEx_MatchDirNameWithDirFromPath
{
    class Program
    {
        static void Main(string[] args)
        {
            var folderPath = "products/MCP/Packages/Music_demo/CONCEPT=HD/";
            var command = "GEOM*products/MCP/Packages/Music_demo/CONCEPT=HD/ FOLDER_EXIST";
            //Regex (".*\\*GEOM\\*([a-zA-Z0-9]+)");
            command = command.Replace("GEOM*", "").Replace(" FOLDER_EXIST", "");
            var result = folderPath.Contains(command);
            Console.WriteLine(result);
        }
    }
}