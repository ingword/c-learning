﻿using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualBasic;

namespace TransferSymbolsAsync
{
    class Program
    {
        internal const string InText = "abc d ef ghi j1223 klmpopqrstvuwxyz";

        static async Task TransferSymbols(Stream source, Stream destination)
        {
            // ваш код по асинхронной фильтрации байтов из source в destination       
            //destination = source;
            byte[] buffer = new byte[1];
            while (source.Position < source.Length)
            {
                await source.ReadAsync(buffer, 0, 1);
                if (97 <= buffer[0] && buffer[0] <= 122)
                {
                    //Console.Write($" {buffer[0]}");
                    await destination.WriteAsync(buffer, 0, buffer.Length);
                }

            }

        }


        static void Main(string[] args)
        {
            var sourceStream = new MemoryStream(Encoding.ASCII.GetBytes(InText));
            var destinationStream = new MemoryStream();
            TransferSymbols(sourceStream, destinationStream).Wait();
            StreamReader reader = new StreamReader(destinationStream);
            destinationStream.Position = 0;
            string outText = reader.ReadToEnd();

            Console.WriteLine(outText);
        }
    }
}
