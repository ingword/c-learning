﻿using System;

namespace YieldExample
{
    public static class Program
    {
        private static System.Collections.Generic.IEnumerator<bool> Puzzle()
        {
            try
            {
                yield return true;
            }
            finally
            {
                System.Console.Write("this is it!");
            }
        }

        public static void Main()
        {
            using (var modal = Puzzle())
            {
            }
        }
    }
}
