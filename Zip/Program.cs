﻿using System;
using System.Collections.Generic;

namespace Zip
{
    static class Program
    {
        public static IEnumerable<TResult> Zip<TL, TR, TResult>
        (
            IEnumerable<TL> left, IEnumerable<TR> right, Func<TL, TR, TResult> glue
        )
        {
            var leftEnum = left.GetEnumerator();
            var rightEnum = right.GetEnumerator();

            while (leftEnum.MoveNext())
            {
                if (rightEnum.MoveNext())
                {
                    yield return glue(leftEnum.Current, rightEnum.Current);

                }
            }
        }
        static void Main(string[] args)
        {
            int[] a = {1, 2, 3, 4};
            int[] b = {1, 2, 3, 4};
            var c = Zip(a, b, (x, y) => x * y);
            foreach (var i in c)
            {
                Console.WriteLine($" {c}");
            }
            Console.WriteLine("Hello World!");
        }
    }
}
